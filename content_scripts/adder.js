(function () {
  /**
   * Check and set a global guard variable.
   * If this content script is injected into the same page again,
   * it will do nothing next time.
   */
  alert("thyuu");
  if (window.hasRun) {
    return;
  }
  window.hasRun = true;



  function add(key2, value2) {

    async function setstorage() {
      try {
        storage = await browser.storage.local.get("storage");
        // console.log(storage);
        try {
          if (typeof storage.storage.key !== 'undefined' && storage.storage.key.length >= 0) {
            // console.log("ch");
            if (storage.storage.key.includes(key2)) {
              storage.storage.value[storage.storage.key.indexOf(key2)] = value2;
            } else {
              storage.storage.key.push(key2);
              storage.storage.value.push(value2);
            }
            // console.log(storage.storage);

            // browser.storage.local.set({ "storage": storage.storage }).then(setstorage);
            browser.storage.local.set({ "storage": storage.storage });
          }

        } catch {
          // console.log("ec");
          key_init = [];
          value_init = [];

          storage = {
            key: key_init,
            value: value_init
          }

          storage.key.push(key2);
          storage.value.push(value2);
          // console.log(storage);
          // browser.storage.local.set({ "storage": storage }).then(setstorage);
          browser.storage.local.set({ "storage": storage });

        }
      } catch (error) {
        console.log(error);
      }
    }
    function gotstorage() {

      // console.log("suc");
      // return 1;
    }

    if (key2 != "date" && key2 != "" && value2 != "") {
      // console.log("thuuu");
      setstorage().then(
        gotstorage
      );
    }
  }

  function remove(key2) {
    async function removestorage() {

      try {
        storage = await browser.storage.local.get("storage");
        // console.log(storage);
        try {
          if (typeof storage.storage.key !== 'undefined' && storage.storage.key.length >= 0) {
            // console.log("ch");
            if (storage.storage.key.includes(key2)) {
              // storage.storage.value[storage.storage.key.indexOf(key2)] = value2;
              // } else {
              //   storage.storage.key.push(key2);
              //   storage.storage.value.push(value2);

              const index = storage.storage.key.indexOf(key2);
              if (index > -1) {
                storage.storage.key.splice(index, 1);
                storage.storage.value.splice(index, 1);
              }
            }
            // console.log(storage.storage);

            // browser.storage.local.set({ "storage": storage.storage }).then(setstorage);
            browser.storage.local.set({ "storage": storage.storage });
          }

        } catch {
          // console.log("ec");
          key_init = [];
          value_init = [];

          storage = {
            key: key_init,
            value: value_init
          }

          // storage.key.push(key2);
          // storage.value.push(value2);
          // console.log(storage);
          // browser.storage.local.set({ "storage": storage }).then(setstorage);
          browser.storage.local.set({ "storage": storage });

        }

      } catch (error) {
        console.log(error);
      }
    }
    removestorage();
  }

  browser.runtime.onMessage.addListener((message) => {
    if (message.command === "add") {
      add(message.key, message.value);
    } else if (message.command === "remove") {
      remove(message.key);
    }
  });
})();