/**
 * CSS to hide everything on the page,
 * except for elements that have the "typer-image" class.
 */
const hidePage = 'body > :not(.typer-image) {display: none;}';

/**
* Listen for clicks on the buttons, and send the appropriate message to
* the content script in the page.
*/
function listenForClicks() {
    // document.addEventListener("load",(e3) => {

    //     alert("thy");
    // });
    // browser.runtime.onMessage.addListener((message) => {
    //   if (message.command === "add") {
    //     // add(message.key, message.value);
    //     // console.log("wrkkk");
    //   }
    // });
    document.addEventListener("click", (e) => {
        // function typer(tabs) {
        //     browser.tabs.sendMessage(tabs[0].id, {
        //         command: "correct",
        //         // beastURL: url
        //     });

        // }
        function adder(tabs) {
            browser.tabs.sendMessage(tabs[0].id, {
                command: "add",
                key: document.getElementById("key").value.trim(),
                value: document.getElementById("value").value.trim(),
            }).then(() => {

                document.getElementById("key").value = "";
                document.getElementById("value").value = "";
            });

        }
        function remover(tabs) {
            browser.tabs.sendMessage(tabs[0].id, {
                command: "remove",
                key: document.getElementById("keyremove").value.trim(),
            }).then(() => {

                document.getElementById("keyremove").value = "";
            });
        }
        function reportError(error) {
            console.error(`Could not typer: ${error}`);
        }
        if (e.target.classList.contains("add")) {
            //     browser.tabs.query({ active: true, currentWindow: true })
            //         .then(typer)
            //         .catch(reportError);
            // } else {

            var key2 = document.getElementById("key").value.trim();
            var value2 = document.getElementById("value").value.trim();
            if (key2 == "date") {
                var x = document.getElementById("dispnokey");
                x.style.display = "block";
            } else if (key2 == "" && value2 == "") {
                var x = document.getElementById("dispnokeyval");
                x.style.display = "block";
            } else if (key2 == "") {
                var x = document.getElementById("dispnokey");
                x.style.display = "block";
            } else if (value2 == "") {
                var x = document.getElementById("dispnoval");
                x.style.display = "block";
            } else {
                var x = document.getElementById("disp");
                x.style.display = "block";

                browser.tabs.query({ active: true, currentWindow: true })
                    .then(adder)
                    .catch(reportError);
            }
            setTimeout(function () { x.style.display = "none"; }, 2000);
        } else if (e.target.classList.contains("remove")) {
            var key2 = document.getElementById("keyremove").value.trim();
            if (key2 == "date") {
                var x = document.getElementById("dispnokeyremove");
                x.style.display = "block";
            } else if (key2 == "") {
                var x = document.getElementById("dispnokeyremove");
                x.style.display = "block";
            } else {
                var x = document.getElementById("dispremove");
                x.style.display = "block";

                browser.tabs.query({ active: true, currentWindow: true })
                    .then(remover)
                    .catch(reportError);
            }
            setTimeout(function () { x.style.display = "none"; }, 2000);

        } else if (e.target.classList.contains("showadder")) {

            var x2 = document.getElementById("add");
            x2.style.display = "block";
            document.getElementById("remove").style.display = "none";

        } else if (e.target.classList.contains("showremover")) {

            var x2 = document.getElementById("remove");
            x2.style.display = "block";
            document.getElementById("add").style.display = "none";
        } else if (e.target.classList.contains("onoff")) {
            if (document.getElementById("onoff").checked) {
                document.getElementById("content").style.display = "block";
            } else {
                document.getElementById("content").style.display = "none";
                document.getElementById("add").style.display = "none";
                document.getElementById("remove").style.display = "none";
            }
        }
    });
}

/**
* There was an error executing the script.
* Display the popup's error message, and hide the normal UI.
*/
function reportExecuteScriptError(error) {
    document.querySelector("#popup-content").classList.add("hidden");
    document.querySelector("#error-content").classList.remove("hidden");
    console.error(`Failed to execute typer content script: ${error.message}`);
}

/**
* When the popup loads, inject a content script into the active tab,
* and add a click handler.
* If we couldn't inject the script, handle the error.
*/
browser.tabs.executeScript({ file: "/content_scripts/adder.js" })
    .then(listenForClicks)
    .catch(reportExecuteScriptError);
// browser.tabs.executeScript(listenForClicks)
//     .catch(reportExecuteScriptError);